
% A = randi(4,4);
% A = [1 2 3 4 ; 3 4 ; 5 6 ];
% A
A = randi([-9 9],5, 3)
% A = [ ( [ 1 2 -2 ])' ( [ 1 -1 4 ])' ]

% A = [1 2 4 ; 0 0 5 ; 0 3 6];


function retval = vl(arg)
  retval = sqrt(arg'*arg);
end

[w k] = size(A);



for j = 1:k
  % printf("j %d\n", j)
  y(:,j) = A(:,j) ;

  for jj = 1:j-1
    y(:,j) = y(:,j) - dot(A(:,j),y(:,jj)) * y(:,jj)/ vl(y(:,jj))**2;
  end

  q(:,j) = y(:,j) / vl(y(:,j));
end

format rat
y
q 
% rats(y,0.1)

for i = 1:k
  for j = i:k
    r(i,j) = q(:,i)' * A(:,j);
  end
end

q*r - A
% inverse(q)-q
% q'*q
% q*q'
