% dane do zadania
y = [ 1.26 ; 1.28 ; 1.32 ; 1.30 ] ;

n = length(y) ;
r = 1 ;

gamma = 0.90 ;

% rozwiązanie
Ey = mean(y) ;

v = Ey - y ;

sigmahat2 = transpose(v) * v / (n - r) ;

% przedział prawostronny
chi2  = chi2inv(1-gamma,n-r) ;
int_r = sigmahat2 * (n-r) / chi2

% przedział dwustronny

gamma_1 = (1+gamma)/2
gamma_2 = (1-gamma)/2
chi2_1 = chi2inv(1-gamma_1,n-r)
chi2_2 = chi2inv(1-gamma_2,n-r)

int_d_l = (n-r)*sigmahat2/chi2_2
int_d_r = (n-r)*sigmahat2/chi2_1
