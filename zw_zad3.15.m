y = [ 2.006 ; 1.992 ; 3.009; 1 ]  ;
A = [1 0 ; 1 0 ; 0 1 ; -1 1 ];
w = [2; 2 ;3 ; 1 ];

L = w - y;

P = diag([1,1,1,1]) 

Xhat = - inverse((transpose(A) * A)) * (transpose(A)*L)

vhat = A * Xhat + L

f = length(y) - length(Xhat)

sigmahat2 = (transpose(vhat) * vhat) / f * 1e6 % przemnożenie przez milion, aby uzyskać wariancję w mm^2 a nie w m^2

gamma = 0.99
int_r = f * sigmahat2 / chi2inv(1-gamma,f)

int_d_l = f * sigmahat2 / chi2inv(1-(1-gamma)/2,f)
int_d_r = f * sigmahat2 / chi2inv(1-(1+gamma)/2,f)
