A = [10, 2, 4 ,5 ; 2 5 6 3 ; 4 3 -1 2 ; 2 3 4 4 ];
b = [ 3 ; 5 ; 6 ;7];

if (det(A) == 0)
  disp "macierz osobliwa"
  exit
end

[w, k] = size(A);

U = [A b];
for j = 1 : k-1
  for i = j+1 : w
    m = U(i,j)/U(j,j);
    U(i,:) = U(i,:) -m * U(j,:);
  end
end
U

x = zeros(w,1) ;
for i = w:-1:1
  x(i) = (U(i,k+1) - U(i,i+1:k)*x(i+1:w))/U(i,i) ;
end

% [x inverse(A)*b x-inverse(A)*b]
