
% -2*x^3 + 6*x^2 - 4

coefficients = [-2 6 0 -4]

roots(coefficients)

a = 2 ;
b =  100 ;

limit = 1e-2

iterations = 0;
while((b-a) > limit)
  iterations += 1 ;

  % fa = -2*a^3+6*a**2 -4;
  fa = polyval(coefficients,a);
  % fb = -2*b^3+6*b**2 -4;
  fb = polyval(coefficients,b);

  c  = (a+b)/2;
  % fc = -2*c^3+6*c**2 -4;
  fc = polyval(coefficients,c);

  if (fa*fc<0)
    b = c;
  else
    a = c;
  end

end
c
iterations
